import { ethers } from "./ethers-5.6.esm.min.js";
import { abi, contractAddress } from "./constants.js";

const connectButton = document.getElementById("connectButton");
const fundButton = document.getElementById("fundButton");
const balanceButton = document.getElementById("getBalanceButton");
const withdrawButton = document.getElementById("withdrawButton");
connectButton.onclick = connect;
fundButton.onclick = fund;
balanceButton.onclick = getBalance;
withdrawButton.onclick = withdraw;

async function connect() {
    if (typeof (window.ethereum !== "undefined")) {
        console.log("Metamask detected!");
        await window.ethereum.request({ method: "eth_requestAccounts" });
        connectButton.innerHTML = "Connected";
    } else {
        connectButton.innerHTML = "please install metamask";
    }
}

async function fund() {
    const ethAmount = document.getElementById("ethAmount").value;
    console.log(`Funding with ${ethAmount}...`);
    if (typeof window.ethereum !== "undefined") {
        //provider/connection to blockchain
        const provider = new ethers.providers.Web3Provider(window.ethereum);
        //signer/wallet
        const signer = provider.getSigner();
        //contract to be funded
        //ABI and Address of contract
        const contract = new ethers.Contract(contractAddress, abi, signer);
        try {
            const transactionResponse = await contract.fund({
                value: ethers.utils.parseEther(ethAmount),
            });
            await listenerForTransactionMine(transactionResponse, provider);
        } catch (error) {
            console.log(error);
        }
    } else {
        fundButton.innerHTML = "Please install MetaMask";
    }
}

async function getBalance() {
    if (typeof window.ethereum !== "undefined") {
        const provider = new ethers.providers.Web3Provider(window.ethereum);
        const balance = await provider.getBalance(contractAddress);
        console.log(ethers.utils.formatEther(balance));
    }
}

async function withdraw() {
    if (typeof window.ethereum !== "undefined") {
        console.log("Withdrawing");
        const provider = new ethers.providers.Web3Provider(window.ethereum);
        const signer = provider.getSigner();
        const contract = new ethers.Contract(contractAddress, abi, signer);
        try {
            const transactionRespone = await contract.withdraw();
            await listenerForTransactionMine(transactionRespone, provider);
        } catch (error) {
            console.log(error);
        }
    }
}

function listenerForTransactionMine(transactionRespone, provider) {
    return new Promise((resolve, reject) => {
        provider.once(transactionRespone.hash, (transactionReceipt) => {
            console.log(
                `Completed ${transactionReceipt.confirmations} confirmations`
            );
            resolve();
        });
    });
}
